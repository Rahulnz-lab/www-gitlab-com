---
layout: handbook-page-toc
title: "President's Club"
description: "Predident's Club at GitLab"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

The [GitLab President's Club Handbook](https://docs.google.com/document/d/1AtizSBnTSSRXKG5a4m23t_OV9lbaQpCqyCFjfS9ctpI/edit?usp=sharing) is in the process of being moved to our GitLab internal handbook.

